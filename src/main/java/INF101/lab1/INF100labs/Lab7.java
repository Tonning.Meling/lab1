package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) {
            grid.remove(row);
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (grid.size() == 0 || grid.get(0).size() == 0) {
            return false; // Lager et tomt rutenet først 
        }

        int expectedSum = calculateRowSum(grid.get(0));

        // løkke for å sjekke radene.
        for (ArrayList<Integer> row : grid) {
            if (calculateRowSum(row) != expectedSum) {
                return false;
            }
        }

        // for-løkke for å sjekke kolonnene
        for (int col = 0; col < grid.get(0).size(); col++) {
            int currentColSum = 0;
            for (ArrayList<Integer> row : grid) {
                currentColSum += row.get(col);
            }
            if (currentColSum != expectedSum) {
                return false;
            }
        }

        return true;
    }
    private static int calculateRowSum(ArrayList<Integer> row) {
        int sum = 0;
    
        for (Integer value : row) {
            sum += value;
    }
    
        return sum;
    }
}