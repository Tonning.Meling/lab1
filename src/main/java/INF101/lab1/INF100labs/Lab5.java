package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (Integer tall : list) {
            int resultat = tall * 2;
            resultList.add(resultat);
        }
        return resultList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeIf(number -> number == 3);
        return list; // denne var ok å forstå. 
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        Set<Integer> uniqueSet = new LinkedHashSet<>(list);
        return new ArrayList<>(uniqueSet); //denne var ok å forstå
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        int size = Math.min(a.size(), b.size());

        for (int i = 0; i < size; i++) {
            a.set(i, a.get(i) + b.get(i));
        } // denne var vrien. Sjekk ut flere youtube tutorials på temaet om jeg finner.
    }

}