package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        crossSum(123);
    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 7;
        while (i <= n) {
            System.out.println(i);
            i += 7;
        }
    }

    public static void multiplicationTable(int n) {
        int i = 1;

        while (i <= n) {
            System.out.print(i + ": ");

            int j = 1;

            while (j <= n) {
                System.out.print(i * j + " ");
                j = j + 1;
            }

            System.out.println();
            i = i + 1;
        }
    }

    public static int crossSum(int num) {
        int digitSum = 0; //Her initialiseres variabelen digitSum til 0.

        while (num != 0) {
            digitSum += num % 10; // Legger til siste siffer.
            num/=10; // Fjerner siste siffer
        }
        return digitSum;
    }
}