package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        findLongestWords("Game", "Action", "Champion");
        isLeapYear(2022);
        isEvenPositiveInt(0);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        // Lager en teller for makslengden
        int maxLength = 0;
        // Finner lengste ord og erstatter variabelen "maxLength" med dette :)
        if (word1.length() > maxLength) {
            maxLength =  word1.length();
        }
        if (word2.length() > maxLength) {
            maxLength = word2.length();
        }
        if (word3.length() > maxLength) {
            maxLength = word3.length();
        }
        // Printer lengste ord eller flere dersom flere er like lange. Hadde det gått an å bruke elif her?
        if (word1.length() == maxLength) {
            System.out.println(word1);
        }
        if (word2.length() == maxLength) {
            System.out.println(word2);
        }
        if (word3.length() == maxLength) {
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        } else {
            return false;
        }
    } //Denne gikk greit :)
}
