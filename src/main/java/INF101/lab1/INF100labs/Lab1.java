package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        task2();
    
    }

    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        // Spør bruker om informasjon ved hjelp av metoden/funksjonen "readInput"
        String name = readInput("Hva er ditt navn?");
        String address = readInput("Hva er din adresse?");
        String postal = readInput("Hva er ditt postnummer og poststed?");
        // Printer navn og adresse
        System.out.println(name + "s adresse er:");
        System.out.println(); // printer tom linje, ikke behov for "" inni parantesen
        System.out.println(name);
        System.out.println(address);
        System.out.println(postal); 
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }


}
